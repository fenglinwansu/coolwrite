/*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%                                                            %%%%%%
%%%%%%       欢迎到www.opencvchina.com下载源代码和资料              %%%%%%
%%%%%%                                                            %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
*/
/*
 * Code written by Lya (GeckoGeek.fr)
 */

#include "opencv/highgui.h"
#include "opencv/cv.h"
 
//#include"linux_serial.h"
#include <stdlib.h>
#include <stdio.h>
#include     <unistd.h>     /*Unix标准函数定义*/
 /*the time server using shared memory */

#include<stdio.h>
#include<sys/shm.h>
#include<time.h>
#include<signal.h>

#define TIME_MEM_KEY 99                  //共享内存 号
#define SEG_SIZE ((size_t)10)
#define oops(m,x) {perror(m); exit(x);}



// Maths methods
#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))  
#define abs(x) ((x) > 0 ? (x) : -(x))
#define sign(x) ((x) > 0 ? 1 : -1)
 
// Step mooving for object min & max
#define STEP_MIN 5
#define STEP_MAX 100 

//#define flag 1  
#define noflag 0  
  
//串口终端接收
//char buf1[255];
//int res = 0;

int seg_id;

int move_flag = 0;
int line_flag = 0;
int temp_fd = 0;

// FILE *fp_color = NULL; //采样颜色指针
 
IplImage *image;
IplImage *temp;
IplImage *remain_image;
IplImage* img2;
 
// Position of the object we overlay
CvPoint objectPos = cvPoint(-1, -1);
	// Next position of the object we overlay
	CvPoint objectNextPos;
CvPoint center = cvPoint(320, 240);
// Color tracked and our tolerance towards it
static int h = 0, s = 0, v = 0, tolerance = 10;
 
void   RotateImage(IplImage *src,IplImage *dst,CvPoint center,float angle,float factor)
 {//以点center为旋转中心，对src旋转angle度并缩放factor倍。
  float m[6];
  CvMat mat=cvMat(2,3,CV_32FC1,m);
  m[0] = (float)(factor*cos(-angle*CV_PI/180.));
  m[1] = (float)(factor*sin(-angle*CV_PI/180.));
  m[2] = center.x;
  m[3] = -m[1];
  m[4] = m[0];
  m[5] = center.y;
  cvSetZero(dst);
  cvGetQuadrangleSubPix(src,dst,&mat);
 }
 
/*
 * Transform the image into a two colored image, one color for the color we want to track, another color for the others colors
 * From this image, we get two datas : the number of pixel detected, and the center of gravity of these pixel
 */
CvPoint binarisation(IplImage* image, int *nbPixels) {
 
	int x, y;
	CvScalar pixel;
	IplImage *hsv, *mask;
	IplConvKernel *kernel;
	int sommeX = 0, sommeY = 0;
	*nbPixels = 0;
 
	// Create the mask &initialize it to white (no color detected)
	mask = cvCreateImage(cvGetSize(image), image->depth, 1);
 
	// Create the hsv image
	hsv = cvCloneImage(image);
	cvCvtColor(image, hsv, CV_BGR2HSV);
 
	// We create the mask
	cvInRangeS(hsv, cvScalar(h - tolerance -1, s - tolerance, 0), cvScalar(h + tolerance -1, s + tolerance, 255), mask);
 
	// Create kernels for the morphological operation
	kernel = cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_ELLIPSE);
 
	// Morphological opening (inverse because we have white pixels on black background)
	cvErode(mask, mask, kernel, 1);   //my replace
	cvDilate(mask, mask, kernel, 1);
//	cvErode(mask, mask, kernel, 1);  
 
	// We go through the mask to look for the tracked object and get its gravity center
	for(x = 0; x < mask->width; x++) {
		for(y = 0; y < mask->height; y++) { 
 
			// If its a tracked pixel, count it to the center of gravity's calcul
			if(((uchar *)(mask->imageData + y*mask->widthStep))[x] == 255) {
				sommeX += x;
				sommeY += y;
				(*nbPixels)++;
			}
		}
	}
 
	// Show the result of the mask image
	cvShowImage("GeckoGeek Mask", mask);
 
	// We release the memory of kernels
	cvReleaseStructuringElement(&kernel);
 
	// We release the memory of the mask
	cvReleaseImage(&mask);
	// We release the memory of the hsv image
    	cvReleaseImage(&hsv);

	// If there is no pixel, we return a center outside the image, else we return the center of gravity
	if(*nbPixels > 0)
		return cvPoint((int)(sommeX / (*nbPixels)), (int)(sommeY / (*nbPixels)));
	else
		return objectPos;//return cvPoint(-1, -1);
}
 

/*
 * Add a circle on the video that fellow your colored object
 */
void addObjectToVideo(IplImage* image, CvPoint objectNextPos, int nbPixels) {
 
	int objectNextStepX, objectNextStepY;

	// Calculate circle next position (if there is enough pixels)
	if (nbPixels > 10) {
	
		// Move step by step the object position to the desired position
		if (abs(objectPos.x - objectNextPos.x) > STEP_MIN) {
			move_flag = 1;
		}
		if (abs(objectPos.y - objectNextPos.y) > STEP_MIN) {
			move_flag = 1;
		}
	// -1 = object isn't within the camera range
	}

	objectPos.x = objectNextPos.x;
	objectPos.y = objectNextPos.y;
	
	// Draw an object (circle) centered on the calculated center of gravity
	if (nbPixels > 10)
		cvDrawCircle(image, objectPos, 15, CV_RGB(255, 0, 0), -1);
 
	// We show the image on the window
	cvShowImage("GeckoGeek Color Tracking", image);
 
}
 
void addObjectToMyVideo(IplImage* image, CvPoint objectNextPos, int nbPixels) {
 
	int objectNextStepX, objectNextStepY;
 /*
	// Calculate circle next position (if there is enough pixels)
	if (nbPixels > 10) {
 
		// Reset position if no pixel were found
		if (objectPos.x == -1 || objectPos.y == -1) {
			objectPos.x = objectNextPos.x;
			objectPos.y = objectNextPos.y;
		}
 
		// Move step by step the object position to the desired position
		if (abs(objectPos.x - objectNextPos.x) > STEP_MIN) {
			objectNextStepX = max(STEP_MIN, min(STEP_MAX, abs(objectPos.x - objectNextPos.x) / 2));
			objectPos.x += (-1) * sign(objectPos.x - objectNextPos.x) * objectNextStepX;
		}
		if (abs(objectPos.y - objectNextPos.y) > STEP_MIN) {
			objectNextStepY = max(STEP_MIN, min(STEP_MAX, abs(objectPos.y - objectNextPos.y) / 2));
			objectPos.y += (-1) * sign(objectPos.y - objectNextPos.y) * objectNextStepY;
		}
 
	objectPos.x = objectNextPos.x;
	objectPos.y = objectNextPos.y;
	// -1 = object isn't within the camera range
	} else {
 
		objectPos.x = -1;
		objectPos.y = -1;
 
	}
 */
	// Draw an object (circle) centered on the calculated center of gravity
	if (nbPixels > 5)
	{
		if( line_flag)
		{
			cvCopy(remain_image, img2, NULL);		//载入上一幅有实际笔迹的图像
			cvLine(img2,objectPos, objectNextPos, CV_RGB(255, 0, 0), 8, -1);
			cvCopy(img2, remain_image, NULL);		//将这一副有实际笔迹绘制的图像保存起来
		}
		else
			{
				cvCopy(remain_image, img2, NULL);		//载入上一幅有实际笔迹的图像
				cvDrawCircle(img2, objectPos, 8, CV_RGB(255, 0, 0), -1);
			}
	}
//		cvDrawCircle(image, objectPos, 15, CV_RGB(255, 0, 0), -1);
	// We show the image on the window
	cvShowImage("my window", img2);
}

/*
 * Get the color of the pixel where the mouse has clicked
 * We put this color as model color (the color we want to tracked)
 */
void getObjectColor(int event, int x, int y, int flags, void *param = NULL) {
 
	// Vars
	CvScalar pixel;
	IplImage *hsv;
 
	if(event == CV_EVENT_LBUTTONUP)	{
 
		// Get the hsv image
		hsv = cvCloneImage(image);
		cvCvtColor(image, hsv, CV_BGR2HSV);
 
		// Get the selected pixel
		pixel = cvGet2D(hsv, y, x);
 
		// Change the value of the tracked color with the color of the selected pixel
		h = (int)pixel.val[0];
		s = (int)pixel.val[1];
		v = (int)pixel.val[2];
 		
 		//fseek(fp_color, 0, SEEK_SET);
 		//fprintf(fp_color,"%d %d %d", h, s, v);
		// Release the memory of the hsv image
    		cvReleaseImage(&hsv);
	}
}
/*
//串口中断程序
void  
signal_handler_IO (int status)  
{  
      memset (buf1, 0, sizeof(buf1));  
      res = read (fd, buf1, 255);  
      printf ("nread=%d,%s\n", res, buf1);  
//    if (res ==1)  
//      STOP = 1;       //stop loop if only a CR was input 
}  
*/
/*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%                                                            %%%%%%
%%%%%%       欢迎到www.opencvchina.com下载源代码和资料              %%%%%%
%%%%%%                                                            %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


*/

 
int main() {


	int  *mem_ptr;    //共享内存
	void close_shm();//关闭共享内存

	

    long fpos;    //临时文件偏移量
    int write_num= 0;  //写入字节数
    
    FILE *fp = NULL; //需要注意
    
    fp = fopen("/tmp/serial", "w+");
    temp_fd = fileno(fp);
    //fp_color = fopen("/home/helei/.coolwrite", "w+");
    if(NULL == fp)
    {
        return -1; //要返回错误代码
    }
    
    	//fscanf(fp_color, "%d %d %d", &h, &s, &v);
	
 	    signal( SIGINT, (void*) close_shm);							//捕捉ctrl+c 信号

	seg_id = shmget(TIME_MEM_KEY, SEG_SIZE,  0777);              //设置共享内存
	if ( seg_id == -1)
		oops("shmget", 1);

	mem_ptr = shmat( seg_id, NULL, 0);                                       //获得共享内存
	if( mem_ptr == (void *) -1)
		oops("shmat", 2);

  	
	// Image & hsvImage
	IplImage *hsv;
	// Video Capture
	CvCapture *capture;
	// Key for keyboard event
	char key;
 
	// Number of tracked pixels
	int nbPixels;

 
	// Initialize the video Capture (200 => CV_CAP_V4L2)
 	capture = cvCreateCameraCapture(0);
 
	// Check if the capture is ok
    	if (!capture) {
		printf("Can't initialize the video capture.\n");
        	return -1;
 	}
 	
 	
	img2=cvCreateImage(cvSize(640,480),IPL_DEPTH_32F,3);  //my code
	remain_image = cvCreateImage(cvGetSize(img2),img2->depth,img2->nChannels); //初始化保存图像
	// Create the windows
	cvNamedWindow("my window",CV_WINDOW_AUTOSIZE); //my code
   	cvNamedWindow("GeckoGeek Color Tracking", CV_WINDOW_AUTOSIZE);
   	cvNamedWindow("GeckoGeek Mask", CV_WINDOW_AUTOSIZE);
	cvMoveWindow("GeckoGeek Color Tracking", 0, 100);
	cvMoveWindow("GeckoGeek Mask", 650, 100);

 
	// Mouse event to select the tracked color on the original image
	cvSetMouseCallback("GeckoGeek Color Tracking", getObjectColor);
	
 	temp=cvCreateImage(cvSize(640,480),8,3); 
	// While we don't want to quit
	while(key != 'Q' && key != 'q') {
 
		// We get the current image 
		image = cvQueryFrame(capture);
		RotateImage(image, temp, center, 180, 1);//图像颠倒180度
		image = temp;
 
		// If there is no image, we exit the loop
		if(!image)
			continue;
		//fscanf(fp_color, "%d %d %d", &h, &s, &v);
		//printf("%d %d %d\n",h, s, v);
		objectNextPos = binarisation(image, &nbPixels);
		
		
		if(*mem_ptr)                                              //如果声音分析程序设置共享内存为1
		{
			
			addObjectToMyVideo(img2, objectNextPos, nbPixels);
			
		}
			
		addObjectToVideo(image, objectNextPos, nbPixels);
		addObjectToMyVideo(img2, objectNextPos, nbPixels);
		line_flag = *mem_ptr;
		
		//通过串口发送数据
//		serial_return = send_serial(objectPos, *mem_ptr, fd);
		if( move_flag == 1)
		{
		write_num = fprintf(fp, "%d %d %d ", objectPos.x, (480 - objectPos.y), *mem_ptr);
		fflush(fp);
		fsync(temp_fd);
		move_flag = 0;
		 }

		//清理重置笔迹
		if (key == 'c'|| key == 'C')
		{
			
			cvSetZero(img2);
			cvSetZero(remain_image);
			fclose(fp);
			fp = fopen("/tmp/serial", "w+");
		
		}
		
		// We wait 10 ms
		key = cvWaitKey(10);
	}
 
	// Destroy the windows we have created
	cvDestroyWindow("GeckoGeek Color Tracking");
	cvDestroyWindow("GeckoGeek Mask");
 
	// Destroy the capture
	cvReleaseCapture(&capture);
 	fclose(fp);		//关闭文件描述符
 	//fclose(fp_color);
	return 0;
 
}

void close_shm()
{
	shmctl( seg_id, IPC_RMID, NULL);
	exit(0);
}


