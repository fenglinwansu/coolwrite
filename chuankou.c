/***************   writer:shopping.w   ******************/
#include <reg52.h>
#include<math.h>
#define uint unsigned int
#define uchar unsigned char

sbit ENA    = P1^0; //产生PWM波
sbit ENB    = P1^1;
sbit INPUT1 = P1^2; //控制口
sbit INPUT2 = P1^3;
sbit INPUT3 = P1^4;
sbit INPUT4 = P1^5;

sbit ENA2    = P3^0; //产生PWM波
sbit ENB2    = P3^1;

static unsigned char count = 0;//串口接收计数的变量

//以下为电机控制函数
int speed = 300;
int f = 15;

uchar k=0,z=0;
uint y;
unsigned char code STEP_TABLE1[4]={0x27,0x17,0x1b,0x2b};//电机转动参数
unsigned char code STEP_TABLE2[4]={0x27,0x2b,0x1b,0x17};//电机转动参数

void delay(uint z) 	  //延时函数：z的单位是ms
{
	uint x,y;
	for(x=z;x>0;x--)
		for(y=125;y>0;y--);
}


void xturnangle(long int angle,unsigned char direction,unsigned int speed) //angle为转过的角度，direction为转动方向
{
	uchar i;

	ENA = 1; //打开输出使能
	ENB = 1;
	switch(direction)
	{
		case 1:		angle = angle*5/9;	  //每转一下1.8度
					for(i=0;i<angle;i++){
						P1 = STEP_TABLE1[z];
						for(y=speed;y>0;y--);	//延时一点时间，让电机内的磁场能够建立
						z++;
						if(z==4)z=0;
					}
					break;
		case 2:		angle = angle*5/9;	  //每转一下1.8度
					for(i=0;i<angle;i++){
						P1 = STEP_TABLE2[k];
						for(y=speed;y>0;y--);	 //延时一点时间，让电机内的磁场能够建立
						k++;
						if(k==4)k=0;
					}
					break;
		default:   break;
	}	
}

void yturnangle(long int angle,unsigned char direction,unsigned int speed) //angle为转过的角度，direction为转动方向
{
	uchar i;

	ENA2 = 1; //打开输出使能
	ENB2 = 1;
	switch(direction)
	{
		case 1:		angle = angle*5/9;	  //每转一下1.8度
					for(i=0;i<angle;i++){
						P3 = STEP_TABLE1[z];
						for(y=speed;y>0;y--);	//延时一点时间，让电机内的磁场能够建立
						z++;
						if(z==4)z=0;
					}
					break;
		case 2:		angle = angle*5/9;	  //每转一下1.8度
					for(i=0;i<angle;i++){
						P3 = STEP_TABLE2[k];
						for(y=speed;y>0;y--);	 //延时一点时间，让电机内的磁场能够建立
						k++;
						if(k==4)k=0;
					}
					break;
		default:   break;
	}	
}
	   
	   
//以下为串口控制函数
signed short x0 = 0, y0 = 0;
signed short x1 = 0, y1 = 0;
signed short xe = 0, ye = 0;

unsigned char receive[8]={0,0,0,0,0,0,0,0};//接收缓存
unsigned char xflag = 0; yflag = 0;
bit uart_flag;//串口接收成功标志
bit move_flag; 

void send(uchar *dis) 
{ 
//    while(*dis!='\0') {
        SBUF=*dis;
        dis++;  
        while(!TI);
        TI=0; 
//    }
} 

void move(void)
{
int E = 0, F = 0;
xe = x1 - x0; //y方向增量
ye = y1 - y0; //y方向增量
xflag = xe;
yflag = ye;
xe = abs(xe);
ye = abs(ye);
E = xe + ye; /* 计算终点判断依据*/

while( E > 0) /* 当E 为0 时结束*/
{
	if( F >= 0)
	{
		if ( (x1-x0) >= 0)
			xturnangle(f, 2, speed);
		else
			xturnangle(f, 1, speed);
		F = F - ye ; //F ＞ = 0 时，驱动X 轴方向运动一步
		E--;
	}
	else
	{
		if( yflag >= 0)
			yturnangle(f, 2, speed);
		else
			yturnangle(f, 1, speed);
		F = F + xe ; /* F ＜ 0 时，驱动Y 轴方向运动一步*/
		E--;
	}
}
}

void serial_init(void) 
{ 
    TMOD=0x20;
    TH1=0xf3;  
    TL1=0xf3;
    TR1=1;
    SM0=0; 
    SM1=1;
    PCON |= 0x80;
    ES=1;
    REN=1;
    EA=1;
}

void main()
{
    serial_init();
    delay(100);
//    send("Receiving From 8051...\r\n");
	//SBUF = 0x40;
/*
	x1 = 0;
	y1 = 100;
	move();
	x0 = x1;
	y0 = y1;
	x1 = 100;
	y1 = 100;
	move();
	x0 = x1;
	y0 = y1;
	x1 = 100;
	y1 = 0;
	move();
	x0 = x1;
	y0 = y1;
	x1 = 0;
	y1 = 0;
	move();
*/

    while(1) 
    {
    	if (uart_flag == 1)
    	{
    	//x0 = x1;
    	//y0 = y1;
    	x1 =receive[2];//receive[3]*256 + receive[2];
    	y1 =receive[4];//receive[5]*256 + receive[4];
    	move_flag = receive[6];
    	
    	uart_flag = 0;
    	
    	if(move_flag == 1)
    		move();
    	ES = 1; //开中断
    	
    	
    	}


	send((receive));
	delay(10);
	send((receive+1));delay(10);
	

	send((receive+2));delay(10);
	send((receive+3));delay(10);
	send((receive+4));delay(10);
	send((receive+5));delay(10);
	send((receive+6));delay(10);
	send((receive+7));delay(10);
	
    }
}
 
void Serial_INT() interrupt 4
{
if(RI)
{
	RI=0; 
	receive[count]=SBUF;
	if(count==0&&receive[count]==0xaa)//同时判断count跟收到的数据
		{
			count=1;ENA = ~ENA ;
		}
	else if(count==1&&receive[count]==0x55)
		{
     			count=2;ENB=~ENB ;
		  }
  	else if(count>=2&&count<7)
  		{
      			count++;
  		}
  	else if(count==7&&receive[count]== 0x0a)//判断校验和，数据多的话是求//和，或者其他的校验方法，也可能是固定的帧尾
 	 	{
    			count=0;
     			uart_flag =1;//串口接收成功标志，为1时在主程序中回复，然后清零
   			ES=0;      //关中断，回复完了再ES=1;
   			
  		}
  	else
  		{
     			count=0;//判断不满足条件就将计数值清零
  		}
}
}
